#include <iostream>
#include <utility>
#include <cmath>

class Dog {
private:
    std::string name = "dog";

public:
    explicit Dog(std::string dogName) : name(std::move(dogName)) {}

    std::string getName() {
        return this->name;
    }

    void setName(std::string newName) {
        this->name = std::move(newName);
    }

    static void bark() {
        std::cout << "Gav or whatever" << std::endl;
    }
};

class Vector {
private:
    int x, y, z;
public:
    Vector() : x(0), y(0), z(0) {}

    Vector(int _x, int _y, int _z) : x(_x), y(_y), z(_z) {}

    double getLength() const {
        double powSum = pow(this->x, 2)
                        + pow(this->y, 2)
                        + pow(this->z, 2);
        return sqrt(powSum);
    }
};

int main() {
    Dog bobby("Bobby");
    std::cout << bobby.getName() << std::endl;
    bobby.setName("Good boy");
    Dog::bark();

    Vector vector(5, 5, 5);
    std::cout << vector.getLength() << std::endl;
    return 0;
}
